package com.gitlab.johnjvester.herokuslack.services;

import com.gitlab.johnjvester.herokuslack.models.Artist;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ArtistService {
    public List<Artist> getAllArtists() {
        return createArtists();
    }

    public Artist getArtistByPosition(int position) {
        return createArtists().get(position);
    }

    private List<Artist> createArtists() {
        List<Artist> artistList = new ArrayList<>();

        artistList.add(new Artist("Eric Johnson"));
        artistList.add(new Artist("Led Zeppelin"));
        artistList.add(new Artist("Rush"));
        artistList.add(new Artist("Tool"));
        artistList.add(new Artist("Toto"));
        artistList.add(new Artist("Van Halen"));
        artistList.add(new Artist("Yes"));

        return artistList;
    }
}
